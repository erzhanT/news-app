import React from 'react';
import Grid from "@material-ui/core/Grid";
import {Box, List, ListItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";

const Comments = ({author, description, id, remove}) => {
    return (
        <Grid container spacing={3} key={id}>
            <Grid item xs={12} sm={6}>
                <Box>
                    <List>
                        <ListItem>
                            <b>{author}</b>: <p>{description}</p>
                        </ListItem>
                        <Button onClick={remove}>Delete</Button>
                    </List>
                </Box>

            </Grid>
        </Grid>
    );
};

export default Comments;