import React from "react";
import './App.css';
import {Container, CssBaseline} from "@material-ui/core";
import {Switch, Route} from 'react-router-dom';
import Layout from "./components/UI/Layout/Layout";
import News from "./containers/News/News";
import NewNews from "./containers/NewNews/NewNews";
import CurrentNews from "./containers/CurrentNews/CurrentNews";
const App = () => {


    return (
        <>
            <CssBaseline/>
            <header>
                <Layout/>
            </header>
            <main>
                <Container maxWidth="xl">
                    <Switch>
                        <Route path="/" exact component={News} />
                        <Route path="/news/new" component={NewNews} />
                        <Route path="/news/:id" component={CurrentNews} />
                    </Switch>
                </Container>
            </main>
        </>
    );
}

export default App;
