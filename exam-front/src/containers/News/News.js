import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography, Button} from "@material-ui/core";
import {Link} from "react-router-dom";
import {deleteNews, fetchNews} from "../../store/actions/newsActions";
import NewsItem from "./NewsItem";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const News = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const news = useSelector(state => state.news.news);
    const loading = useSelector(state => state.news.newsLoading);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch, news.length]);


    const deleteHandler = (id) => {
        dispatch(deleteNews(id));
        dispatch(fetchNews());
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">News</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/news/new">Add news</Button>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : news.map(newsItem => (
                    <NewsItem
                        key={newsItem.id}
                        id={newsItem.id}
                        title={newsItem.title}
                        date={newsItem.date}
                        image={newsItem.image}
                        remove={() => deleteHandler(newsItem.id)}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default News;