import React from 'react';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import {Link} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia} from "@material-ui/core";

import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: "56.25%"
    },
    buttonColor: {
        color: "#dd2c00",
    }
});





const NewsItem = ({title,date, image, id, remove}) => {
    const classes = useStyles();




    return (
        <Grid item xs>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardContent>
                    {date}
                </CardContent>
                <CardMedia
                image={'http://localhost:8000/uploads/' + image}
                title={title}
                className={classes.media}
                />
                <CardActions>
                    <IconButton component={Link} to={'/news/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
                <CardActions>
                    <Button className={classes.buttonColor} variant="contained" onClick={remove}>Delete</Button>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default NewsItem;