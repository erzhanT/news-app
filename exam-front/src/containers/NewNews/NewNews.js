import React from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import NewsForm from "../../components/NewsForm/NewsForm";
import {createNews} from "../../store/actions/newsActions";

const NewNews = ({history}) => {
    const dispatch = useDispatch();

    const onNewsFormSubmit = async newsData => {
       await dispatch(createNews(newsData));
       history.push('/');
    }


    return (
        <Grid container direction={"column"}>
            <Grid item xs>
                <Typography variant={'h4'}>New News</Typography>
            </Grid>
            <Grid item xs>
                <NewsForm  onSubmit={onNewsFormSubmit}/>
            </Grid>
        </Grid>
    );
};

export default NewNews;