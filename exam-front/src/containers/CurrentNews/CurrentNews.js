import React, {useEffect, useState} from 'react';
import {Card, CardContent, CardHeader, Grid, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getOneNews} from "../../store/actions/newsActions";
import {createComments, deleteComment, fetchComments} from "../../store/actions/commentsActions";
import Button from "@material-ui/core/Button";
import Comments from "../../components/Comments/Comments";

const CurrentNews = ({match}) => {

    const dispatch = useDispatch();
    const oneNews = useSelector(state => state.news.oneNews);
    const comments = useSelector(state => state.comments.comments)

    const [comment, setComment] = useState({
        news_id: match.params.id,
        author: '',
        description: ''

    });

    useEffect(() => {
        dispatch(fetchComments());
    }, [dispatch]);

    const submitFormHandler = (e) => {
        e.preventDefault();
        dispatch(createComments(comment));
        dispatch(fetchComments());
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setComment(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    useEffect(() => {
        dispatch(getOneNews(match.params.id))
    }, [dispatch, match.params.id]);

    const deleteHandler = (id) => {
        dispatch(deleteComment(id));
        dispatch(fetchComments());
    };
    return (
        <>
            <Grid item xs key={oneNews.id}>
                <Card>
                    <CardHeader title={oneNews.title}/>
                    <CardContent>
                        <Typography content={'p'}>
                            <strong>
                                Date: {oneNews.date}
                            </strong>
                        </Typography>
                    </CardContent>
                    <CardContent>
                        <Typography content={'p'}>
                            Description: {oneNews.description}
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            {comments.map(comment => {
                if (oneNews.id === comment.news_id) {
                    return (
                        <Comments
                            author={comment.author}
                            description={comment.description}
                            key={comment.id}
                            remove={() => deleteHandler(comment.id)}
                        />
                    )
                }
            })}
            <form onSubmit={submitFormHandler}>
                <Grid container direction={"column"} spacing={2}>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            variant={"outlined"}
                            id={'price'}
                            label={'Author'}
                            name={'author'}
                            value={comment.author}
                            onChange={inputChangeHandler}
                        />

                    </Grid>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            multiline
                            rows={3}
                            variant={"outlined"}
                            id={'description'}
                            label={'Description'}
                            name={'description'}
                            value={comment.description}
                            onChange={inputChangeHandler}
                            required
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type={"submit"} color={"primary"} variant={'contained'}>
                            Send
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default CurrentNews;