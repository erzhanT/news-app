import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const GET_NEWS_INFO = 'GET_NEWS_INFO'

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const getNewsInfo = data => ({type: GET_NEWS_INFO, data});


export const fetchNews = () => {
    return async dispatch => {
        try {
            dispatch(fetchNewsRequest());
            const response = await axiosApi.get('/news');
            dispatch(fetchNewsSuccess(response.data));
        } catch (e) {
            dispatch(fetchNewsFailure());
            NotificationManager.error('Could not fetch news');
        }
    };
};

export const createNews = newsData => {
    return async dispatch => {
        await axiosApi.post('/news', newsData);
        dispatch(createNewsSuccess());
    };
};

export const getOneNews = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/news/' + id);
            dispatch(getNewsInfo(response.data))
            console.log(response.data);
        } catch (e) {
            console.log(e);
        }
    }
};

export const deleteNews = id => {
    return async dispatch => {
        try {
            await axiosApi.delete('/news/' + id);
            NotificationManager.success('News was deleted');
        } catch (e) {
            console.log(e)
        }
    }
}
