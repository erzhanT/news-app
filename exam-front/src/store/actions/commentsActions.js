import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const CREATE_COMMENTS_SUCCESS = 'CREATE_NEWS_SUCCESS';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const createCommentsSuccess = () => ({type: CREATE_COMMENTS_SUCCESS});

export const fetchComments = () => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());
            const response = await axiosApi.get('/comments');
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsFailure());
            NotificationManager.error('Could not fetch Comments');
        }
    };
};

export const createComments = commentsData => {
    return async dispatch => {
        await axiosApi.post('/comments', commentsData);
        dispatch(createCommentsSuccess());
    };
};

export const deleteComment = id => {
    return async dispatch => {
        try {
            await axiosApi.delete('/comments/' + id);
            NotificationManager.success('Comment was deleted');
        } catch (e) {
            console.log(e)
        }
    }
}
