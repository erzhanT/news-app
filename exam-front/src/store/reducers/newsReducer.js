import {
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    GET_NEWS_INFO,
} from "../actions/newsActions";

const initialState = {
    news: [],
    oneNews: {},
    newsLoading: false,
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, newsLoading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, newsLoading: false, news: action.news};
        case FETCH_NEWS_FAILURE:
            return {...state, newsLoading: false};
        case GET_NEWS_INFO:
            return {...state, oneNews: action.data}
        default:
            return state;
    }

};
export default newsReducer;