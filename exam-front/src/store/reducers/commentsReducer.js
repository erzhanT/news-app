import {
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS,
} from "../actions/commentsActions";

const initialState = {
    comments: []
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        case FETCH_COMMENTS_FAILURE:
            return {...state};
        default:
            return state;
    }
};

export default newsReducer;