create schema turdumambetov_erzhan_news collate utf8_general_ci;

use turdumambetov_erzhan_news;

create table news
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         not null,
    date        datetime     null,
    image       varchar(255) null
);

create table comments
(
    id          int auto_increment
        primary key,
    author      varchar(255) null,
    description text         null,
    news_id     int          null,
    constraint comments_news_id_fk
        foreign key (news_id) references news (id)
            on update cascade on delete cascade
);

