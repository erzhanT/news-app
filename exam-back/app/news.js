const path = require('path');
const express = require('express');
const mysqlDb = require('../mysqlDb');
const config = require('../config');
const multer = require('multer');
const {nanoid} = require('nanoid');

const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const [news] = await mysqlDb.getConnection().query('SELECT * FROM news');
    res.send(news)
});

router.get('/:id', async (req, res) => {
    const [news] = await mysqlDb.getConnection().query('SELECT * FROM news WHERE id = ?', [req.params.id]);
    const newsItem = news[0];
    res.send(newsItem);
});

router.post('/', upload.single('image'), async (req, res) => {
    const newsItem = req.body;
    const date = new Date();
    if (newsItem.title === '' || newsItem.description === '') {
        res.status(400).send({"error": "Title and id can not be empty"});
    } else {
        if (req.file) {
            newsItem.image = req.file.filename;
        }
        const [result] = await mysqlDb.getConnection().query(
            'INSERT INTO news (title , description, date, image) VALUES (?, ?, ?, ?)',
            [newsItem.title, newsItem.description, date, newsItem.image]
        );
        res.send({...newsItem, id: result.insertId});
    }

});

router.delete('/:id', async (req, res) => {
    const [news] = await mysqlDb.getConnection().query('DELETE FROM news WHERE id = ?', [req.params.id]);
    const newsItem = news[0]
    res.send(newsItem);
});





module.exports = router;