const express = require('express');
const mysqlDb = require('../mysqlDb');


const router = express.Router();


router.get('/', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query('SELECT * FROM comments');
    res.send(comments)
});

router.post('/', async (req, res) => {
    const comment = req.body;
    const date = new Date();
    if (comment.description === '') {
        res.status(400).send({"error": "Description can not be empty"});
    } else if (comment.author === '') {
        comment.author = 'Anonymous';
        const [result] = await mysqlDb.getConnection().query(
            'INSERT INTO comments (author , description, news_id) VALUES (?, ?,?)',
            [comment.author, comment.description, comment.news_id]
        );
        res.send({...comment, id: result.insertId});
    } else {
        const [result] = await mysqlDb.getConnection().query(
            'INSERT INTO comments (author , description, news_id) VALUES (?, ?, ?)',
            [comment.author, comment.description, comment.news_id]
        );
        res.send({...comment, id: result.insertId});
    }
});

router.delete('/:id', async (req, res) => {
    const [comments] = await mysqlDb.getConnection().query('DELETE FROM comments WHERE id = ?', [req.params.id]);
    const comment = comments[0]
    res.send(comment);
});


module.exports = router;